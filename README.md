## Перед началом работы

У вас должна быть установлена node 18 версии. Чтобы проверить что она установилась, выполните в консоли

```
node --version
```

Команда должна вывести версию установленной ноды. Вместе с нодой устанавливается менджер пакетов npm, проверьте что он установился командой

```
npm --version
```

## Установка зависимостей

```
npm install
```

## Ссылки приложения

* cats-api-like - https://meowle.fintech-qa.ru/api/likes (Swagger https://meowle.fintech-qa.ru/api/likes/api-docs-ui/)
* cats-api-photo - https://meowle.fintech-qa.ru/api/photos (Swagger https://meowle.fintech-qa.ru/api/photos/api-docs-ui/)
* cats-api-core - https://meowle.fintech-qa.ru/api/core (Swagger https://meowle.fintech-qa.ru/api/core/api-docs-ui/)

## Запуск тестов

Команда для запуска тестов на стенде:

```
npm run test:stand
```

## Запуск allure

Если вы хотите запустить локально allure server, на вашем комьютере должна быть установлена java.

- Скачать java для windows https://www.java.com/download/ie_manual.jsp
- Как установить allure на windows https://www.youtube.com/watch?v=xdjN-4UxL1c&t=14s

Сервер запускается командой:
```
allure serve
```

## Правила выполнения домашки для студентов

- Работать нужно в своем форке, сделанном от репы https://gitlab.com/meowle/cats-api/-/forks
- В название форка выберите свой никнейм в Gitlab
- Код пушить прямо в мастер СВОЕГО форка
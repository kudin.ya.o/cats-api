const { Pool } = require('pg');
const { pgUser, pgPass, pgDb, pgHost, pgPort } = require('./configs.js');

const pool = new Pool({
  user: pgUser,
  database: pgDb,
  password: pgPass,
  host: pgHost,
  port: pgPort,
  connectionTimeoutMillis: 5000,
});

pool.on('error', (err) => {
  console.error('Error database', err);
  process.exit(-1);
});

/**
 * Добавляем котов в БД
 */
async function addCats(cat) {
  return (
    await pool.query(
      'INSERT INTO Cats(name, description, gender) VALUES ($1, $2, $3) RETURNING *',
      [cat.name, cat.description, cat.gender]
    )
  ).rows[0];
}

/**
 * Возвращаем всех котов
 */
async function allCats(gender) {
  const queryAll = 'SELECT * FROM Cats ORDER BY LOWER(name)';
  const queryWithGender = 'SELECT * FROM Cats WHERE gender = $1 ORDER BY LOWER(name)';

  return (await pool.query(...(gender ? [queryWithGender, [gender]] : [queryAll]))).rows;
}

async function allByLetter(limit, sort, gender) {
  limit = Number(limit);
  const limitRow = limit ? limit : 10;
  let query = `select id, name, description, tags, gender, likes, dislikes, count_by_letter from
(
SELECT *, SUBSTRING(name, 1, 1) as first_letter, Rank()
          over (Partition BY SUBSTRING(name, 1, 1)
                ORDER BY name ASC) AS rank
        FROM cats
        ORDER BY name ${sort}
        ) tmp
    INNER JOIN 
    (select SUBSTRING(name, 1, 1) as first_letter, 
    count(*) as count_by_letter from cats GROUP BY SUBSTRING(name, 1, 1)) as tmp_2
        ON tmp.first_letter = tmp_2.first_letter
    WHERE rank <=${limitRow}`;
  if (gender !== undefined) {
    query += ` AND tmp.gender = '${gender}'`;
  }
  return (await pool.query(query)).rows;
}

/**
 * Поиск котов по указанным параметрам в БД
 * @param {*} searchParams - список параметров для поиска, переданные от клиента (имя, пол (м,ж, унисекс))
 */
async function findCatsByParams(searchParams) {
  const catName = '%' + searchParams.name.replace(/е/gi, '[е|ё]') + '%';
  const catGender = searchParams.gender;

  // logger.info(`searching cats by name: ${catName} and gender: ${searchParams.gender}`);

  const queryWithGender =
    'SELECT * FROM Cats WHERE LOWER(name) SIMILAR TO LOWER($1) AND gender = $2 ORDER BY LOWER(name)';
  const queryWithoutGender =
    'SELECT * FROM Cats WHERE LOWER(name) SIMILAR TO LOWER($1) ORDER BY LOWER(name)';

  return (
    await pool.query(
      ...(catGender ? [queryWithGender, [catName, catGender]] : [queryWithoutGender, [catName]])
    )
  ).rows;
}

async function findCatById(catId) {
  const selectResult = await pool.query('SELECT * FROM Cats WHERE id = $1', [catId]);
  if (selectResult.rows.length === 0) {
    throw { code: 404, message: `Кот с id=${catId} не найден` };
  }
  return selectResult.rows[0];
}

async function findCatByNamePattern(catName, limit, offset) {
  limit = Number(limit);
  offset = Number(offset);
  const limitRow = limit ? limit : 10;
  const offsetRow = offset ? offset : 0;

  const selectResult = await pool.query(
    `SELECT * FROM Cats WHERE LOWER(name) LIKE LOWER ($1) 
      ORDER BY LOWER(name) ASC OFFSET ${offsetRow} LIMIT ${limitRow}`,
    [catName + '%']
  );
  if (selectResult.rows.length === 0) {
    throw { code: 404, message: `Не найдено` };
  }
  return selectResult.rows;
}

/**
 * Сохранение описания кота в БД
 * @param {*} catId - идентификатор кота, отправленный клиентом
 * @param {*} catDescription - описание кота
 */
async function saveCatDescription(catId, catDescription) {
  const updateResult = await pool.query(
    'UPDATE Cats SET description = $1 WHERE id = $2 RETURNING *',
    [catDescription, catId]
  );
  if (updateResult.rows.length === 0) {
    throw { code: 404, message: `Кот с id=${catId} не найден` };
  }
  return updateResult.rows[0];
}

/**
 * Поиск правил валидации в БД
 */
async function findCatsValidationRules(rule) {
  return (await pool.query('SELECT * FROM Cats_Validations WHERE type = $1 ORDER BY id', [rule]))
    .rows;
}

/**
 * Поиск правил валидации в БД
 */
async function findCatsValidationAllRules() {
  return (await pool.query('SELECT * FROM Cats_Validations ORDER BY id')).rows;
}

/**
 * Удаления кота из бд
 * @param catId
 * @returns {*|query|void|Promise<PermissionStatus>}
 */
async function removeCats(catId) {
  const removeResult = await pool.query('DELETE FROM Cats WHERE id = $1 RETURNING *', [catId]);
  if (removeResult.rows.length === 0) {
    throw { code: 404, message: `Кот с id=${catId} не найден` };
  }
  return removeResult.rows[0];
}

module.exports = {
  pool,
  addCats,
  allCats,
  findCatsByParams,
  findCatById,
  findCatByNamePattern,
  saveCatDescription,
  findCatsValidationRules,
  removeCats,
  findCatsValidationAllRules,
  allByLetter,
};

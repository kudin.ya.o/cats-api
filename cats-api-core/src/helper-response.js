const boom = require('boom');
const { logger } = require('./logger');

function sendResponse(req, res, msg) {
  logger.info({ req: req, res: { status: 200, json: msg } });
  return res.status(200).send(msg);
}

function sendError(req, res, err) {
  if (err.hasOwnProperty('code')) {
    switch (err.code) {
      case 404: {
        logger.error({
          req: req,
          res: { status: err.code, json: boom.notFound(err.message) },
        });
        return res.status(err.code).json(boom.notFound(err.message));
      }
      case 400: {
        logger.error({
          req: req,
          res: { status: err.code, json: boom.badRequest(err.message) },
        });
        return res.status(err.code).json(boom.badRequest(err.message));
      }
      case 500: {
        logger.error({
          req: req,
          res: {
            status: err.code,
            json: boom.internal(msg, err.stack || err.message),
          },
        });
        return res.status(err.code).json(boom.internal(msg, err.stack || err.message));
      }
    }
  } else {
    const jsonMessage = boom.internal('Ошибка сервера', err.stack || err.message);
    logger.error({ req: req, res: { status: 500, json: jsonMessage } });
    return res.status(500).json(jsonMessage);
  }
}

module.exports = {
  sendResponse,
  sendError,
};

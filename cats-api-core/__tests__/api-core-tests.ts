import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Кототест', description: '', gender: 'male' }];

let catId;

const fakeId = 'fakeId';

const descriptionText = 'Какое-то очень нереально крутое описание кота';

const HttpClient = Client.getInstance();

describe('API Core', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Поиск существующего котика по id', async () => {
    const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
      responseType: 'json'
    });
    expect(response.statusCode).toEqual(200);

    expect((response.body as any).cat).toEqual({
      id: catId,
      ...cats[0],
      tags: null,
      likes: 0,
      dislikes: 0
    });
  });

  it('Поиск некорректного id котика вызывает ошибку', async () => {
    await expect(
      HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
        responseType: 'json',
      })
    ).rejects.toThrowError('Response code 400 (Bad Request)');
  });

  it('Добавление описания котику', async () => {
    const response = await HttpClient.post(`core/cats/save-description`, {
      responseType: 'json',
      json: {
        catId: catId,
        catDescription: descriptionText
      },
    });

    cats[0].description = descriptionText;

    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
      id: catId,
      ...cats[0],
      tags: null,
      likes: 0,
      dislikes: 0,
    });
  });

  it('Метод получение списка котов сгруппированных по группам', async () => {
    const response = await HttpClient.get('core/cats/allByLetter', {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
      groups: expect.arrayContaining([
        expect.objectContaining({
          title: expect.any(String),
          cats: expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              description: expect.any(String),
              tags: null,
              gender: expect.any(String),
              likes: expect.any(Number),
              dislikes: expect.any(Number),
              count_by_letter: expect.any(String),
            }),
          ]),
          count_in_group: expect.any(Number),
          count_by_letter: expect.any(Number),
        }),
      ]),      
      count_output: expect.any(Number),
      count_all: expect.any(Number),
    });
  });
});